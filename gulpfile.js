'use strict';

var gulp = require('gulp');

// load plugins
var $ = require('gulp-load-plugins')();


gulp.task('styles', function () {
    return gulp.src('app/styles/**/main.styl')
        .pipe($.stylus() )
        .pipe(gulp.dest('.tmp/styles'));

});

gulp.task('scripts', function () {
    return gulp.src('app/js/**/*.js')
        .pipe($.jshint())
        .pipe($.jshint.reporter(require('jshint-stylish')))
        .pipe(gulp.dest('.tmp/js'))
        .pipe($.size())
});

gulp.task('images', function () {
    //return;

    return gulp.src('app/img/**/*')
    //     .pipe($.imagemin({
    //         optimizationLevel: 3,
    //         progressive: true,
    //         interlaced: true,
    //     }))
        .pipe(gulp.dest('dist/img'))
    //     .pipe($.size());
});
//''templates','styles', 'scripts'

gulp.task('fonts', function () {
    return gulp.src('app/fonts/**/*')
        .pipe(gulp.dest('.tmp/fonts'))
        //.pipe(gulp.dest('../public_html/application/themes/hargreave_hale/fonts'))
        .pipe($.size())
});

gulp.task('compile', ['templates','styles','scripts'], function () {
  
    var assets = $.useref.assets({searchPath: ['.tmp','app'] });
    var gulpif = require('gulp-if');
    var minifyHtml = require('gulp-minify-html');

    return gulp.src('.tmp/*.html')
        .pipe( assets )
        .pipe( gulpif( '*.js', $.uglify() ))
        .pipe( gulpif( '*.css', $.csso() ))
        .pipe( assets.restore() )
        .pipe( $.useref() )
        //.pipe( minifyHtml( {empty:true} ) )
        .pipe(gulp.dest('dist'))
        .pipe($.size());
});




gulp.task('clean', function(cb) {
    var del = require('del');
    del.sync(['.tmp','dist']);
    cb();
});


gulp.task('connect', function () {
    var serveStatic = require('serve-static');
    var connect = require('connect')
    var app = connect();


    $.connect.server({
        root: [ 'app','.tmp' ],
        port:9000,
        livereload: true,

    });
});


gulp.task('open', ['connect', 'styles'], function () {
    require('opn')('http://localhost:9000');
});

gulp.task('templates', function(){
    return gulp.src( 'app/*.jade')
        .pipe( $.jade({pretty: true}) )
        .pipe( gulp.dest('.tmp') )
        .pipe( $.size() );

});

gulp.task('copy', function () {
    return gulp.src( [ 'app/fonts/**/*', 'app/downloads/**/*' ],{ base: 'app/' } )
        .pipe( gulp.dest('dist') )
        .pipe( $.size() );
})


// inject bower components
gulp.task('wiredep', function () {
    var wiredep = require('wiredep').stream;

    gulp.src('app/*.jade')
        .pipe(wiredep({
            directory: 'app/bower_components'
        }))
        .pipe(gulp.dest('app'));
});


gulp.task('build', ['clean','compile','images','copy','fonts']);

gulp.task('default', ['watch','open'] );

gulp.task('watch', ['templates','styles','scripts','fonts','connect','open'], function () {
   
    gulp.watch([
        '.tmp/*.html',
        '.tmp/styles/**/*.css',
        '.tmp/js/**/*.js',
        'app/img/**/*'
    ]).on('change', function (file) {
        gulp.src( file.path)
            .pipe( $.connect.reload() );
    });

    
    gulp.watch('app/**/*.jade', ['templates']);
    gulp.watch('app/styles/**/*.styl', ['styles']);
    gulp.watch('app/js/**/*.js', ['scripts']);
    gulp.watch('app/img/**/*', ['images']);
    gulp.watch('bower.json', ['wiredep']);
    //gulp.watch('bower.json', ['wiredep']);
});

