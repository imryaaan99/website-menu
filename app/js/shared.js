!function(){
	'use strict';

	function menuBar(view){
			this.view = view;
			this.clickable = $(".colorBarNav,#headerOverlay, #header");
			this.navigationBar = $("#navigation");
			this.navigationContainer = $("#navigationContainer");
			this.navigation = $("#nav-toggle");
			this.logo = $("#logoSubContainer");
			this.header = $("#header");
			this.list = $("#listItems");
			this.init();
		}
		menuBar.prototype = {
			init : function(e){
				this.anchorSections();
				var self = this;

				var mq = window.matchMedia( "(max-width: 767px)" );
				if (mq.matches) {
						$("#header").animate({opacity:1}, 500);
						$("#header").css({ position: "fixed",	top: 0});
						self.menuAnimationStart();
						$("#nav-toggle").css({ position: "fixed",	top: 17});
						$("#nav-toggle span").addClass("nav-toggle-black");
				};

				self.clickable.on("click", function(e){
					self.click();
				});

				self.menuAnimationStart();

				$(window).scroll(function(){
					if ( $(window).width() > 768 ) {
						( self.isOverlap("#first","nav-toggle span") == true) ? $("#nav-toggle span").addClass("nav-toggle-white") : $("#nav-toggle span").removeClass("nav-toggle-white");
						( self.isOverlap("#second","nav-toggle span") == true) ? $("#nav-toggle span").addClass("nav-toggle-blue") : $("#nav-toggle span").removeClass("nav-toggle-blue");
						( self.isOverlap("#third","nav-toggle span") == true) ? $("#nav-toggle span").addClass("nav-toggle-red") : $("#nav-toggle span").removeClass("nav-toggle-red");
					}
				});

				var timeOut = null;
				var func = function() {
					self.resize();
				};
				window.onresize = function(){
					if(timeOut != null) clearTimeout(timeOut);
					timeOut = setTimeout(func, 500);
				};
			},
			click : function(){
				var self = this;
				self.navigationBar.toggleClass("selected");
				self.navigation.toggleClass("active");
				self.logo.toggleClass("move");
				self.header.toggleClass("headerOpen");
				self.overlay();

				self.navigationBar.hasClass("selected") ? self.list.velocity({opacity: 1}, {delay: 600, duration: 1000}) : self.list.velocity({opacity: 0}, {duration: 100});
			},
			menuAnimationStart : function(){
				var self = this;
				self.clickable.velocity({left: 0}, {duration: 1000});
				self.logo.velocity({left: 0}, {delay: 250, duration: 500});
				self.navigation.velocity({opacity: 1}, {delay: 1000, duration: 500});
			},
			isOverlap : function(idOne,idTwo){
				var objOne=$(idOne),
						objTwo=$("#"+idTwo),
						offsetOne = objOne.offset(),
						offsetTwo = objTwo.offset(),
						topOne=offsetOne.top,

						topTwo=offsetTwo.top,
						leftOne=offsetOne.left,
						leftTwo=offsetTwo.left,
						widthOne = objOne.width(),
						widthTwo = objTwo.width(),
						heightOne = objOne.height(),
						heightTwo = objTwo.height();
				var leftTop = leftTwo > leftOne && leftTwo < leftOne+widthOne
								&& topTwo > topOne && topTwo < topOne+heightOne,
						rightTop = leftTwo+widthTwo > leftOne && leftTwo+widthTwo < leftOne+widthOne
								&& topTwo > topOne && topTwo < topOne+heightOne,
						leftBottom = leftTwo > leftOne && leftTwo < leftOne+widthOne
								&& topTwo+heightTwo > topOne && topTwo+heightTwo < topOne+heightOne,
						rightBottom = leftTwo+widthTwo > leftOne && leftTwo+widthTwo < leftOne+widthOne
								&& topTwo+heightTwo > topOne && topTwo+heightTwo < topOne+heightOne;
				return leftTop || rightTop || leftBottom || rightBottom;
			},
			anchorSections : function(){
				$('.colorBarNav').click(function() {
					if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
							|| location.hostname == this.hostname) {

							var target = $(this.hash);
							target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
								 if (target.length) {
									 $('html,body').animate({
											 scrollTop: target.offset().top
									}, 1000);
									return false;
							}
					}
				});
			},
			overlay : function() {
				$("#headerOverlay").toggleClass("up");
				$("#headerOverlay").hasClass("up") ? $("#headerOverlay").velocity({opacity: 0.7}, {delay: 100, duration: 500, display: 'block'}) : $("#headerOverlay").velocity({opacity: 0}, {duration: 500, display: 'none'})
			},
			resize : function(e){
				console.log("resize");
				location.reload();
			},
		};
		var ProjectionSlider = new menuBar($('#header'));


}();
